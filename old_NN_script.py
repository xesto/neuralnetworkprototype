#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 27 15:14:04 2018

@author: afiny
"""

from __future__ import print_function

import tensorflow as tf

learning_rate = 0.001
num_steps = 400
display_step = 25
batch_size = 5

# Network Parameters
n_hidden_1 = 256 # 1st layer number of neurons
n_hidden_2 = 1530 # 2nd layer number of neurons
n_hidden_3 = 1530
#n_hidden_4 = 1530
#n_hidden_5 = 256
num_input = 15*18 #85*18 = minimum_length*18
num_classes = 4 # 
thumb = np.load("thumb.npy")
thumb_labels =  [[0] for i in range(len(thumb))]
#thumb_labels = np.squeeze(thumb_labels)
stacked_thumb = [np.hstack(thumb[i]) for i in range(len(thumb) ) ]

peace = np.load("peace.npy")
peace_labels =  [[1] for i in range(len(peace))]
#peace_labels = np.squeeze(peace_labels)
stacked_peace = [np.hstack(peace[i]) for i in range(len(peace) ) ]


five_fingers = np.load("five_fingers.npy")
five_fingers_labels = [[2] for i in range(len(five_fingers))]   
#five_fingers_labels = np.squeeze(five_fingers_labels)
stacked_five_fingers = [np.hstack(five_fingers[i]) for i in range(len(five_fingers) ) ]


no_pinky = np.load("no_pinky.npy")
no_pinky_labels = [[3] for i in range(len(no_pinky))] 
#no_pinky_labels = np.squeeze(no_pinky_labels)
stacked_no_pinky = [np.hstack(no_pinky[i]) for i in range(len(no_pinky)) ]

###Training Dataset
labels = np.vstack((thumb_labels[0:30], peace_labels[0:30], 
                    five_fingers_labels[0:30], no_pinky_labels[0:30]))
labels = np.squeeze(labels) # 

stacked_features = np.vstack((stacked_thumb[0:30], stacked_peace[0:30], 
                              stacked_five_fingers[0:30], stacked_no_pinky[0:30]))

stacked_features = np.array(stacked_features, dtype = np.float32)

stacked_test_features = np.vstack((stacked_thumb[30:-1], stacked_peace[30:-1], 
                              stacked_five_fingers[30:-1], stacked_no_pinky[30:-1]))

test_labels = np.vstack((thumb_labels[30:-1], peace_labels[30:-1], 
                    five_fingers_labels[30:-1], no_pinky_labels[30:-1]))
test_labels = np.squeeze(test_labels)



# tf Graph 
X = tf.placeholder("float", [None, num_input])
Y = tf.placeholder("float", [None, num_classes])

weights = {
    'h1': tf.Variable(tf.random_normal([num_input, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    #'h3': tf.Variable(tf.random_normal([n_hidden_2, n_hidden_3])),
    #'h4': tf.Variable(tf.random_normal([n_hidden_4, n_hidden_5])),
    'out': tf.Variable(tf.random_normal([n_hidden_2, num_classes]))
}
biases = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    #'b3': tf.Variable(tf.random_normal([n_hidden_3])),
    #'b4': tf.Variable(tf.random_normal([n_hidden_4])),

    'out': tf.Variable(tf.random_normal([num_classes]))
}


def neural_net(x):
    layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
    layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
    out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
    return out_layer

# Construct model
logits = neural_net(X)
prediction = tf.nn.softmax(logits)

# Define loss and optimizer
loss_op = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(
    logits=logits, labels=Y))
optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate, initial_accumulator_value=0.1)
train_op = optimizer.minimize(loss_op, global_step=tf.train.get_global_step())

# Evaluate model
correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

init = tf.global_variables_initializer()

dataset = tf.data.Dataset.from_tensor_slices( (stacked_features, labels)).shuffle(buffer_size = 40000)
dataset = dataset.batch(batch_size)

with tf.Session() as sess:
    sess.run(init)


"""
    Note: the itrator was buggy, need to go bback and fix it
"""
#
#with tf.Session() as sess:
#
#    # Run the initializer
#    sess.run(init)
#    data_batch = dataset_iter.next()
#    for step in range(1, num_steps+1):
#        x_batch =  data_batch[0]
#        y_batch =  tf.cast(data_batch[1], dtype=tf.int64)
#        # Run optimization op (backprop)
#        sess.run(train_op, feed_dict={X: x_batch, Y: y_batch})
#        if step % display_step == 0 or step == 1:
#            # Calculate batch loss and accuracy
#            loss, acc = sess.run([loss_op, accuracy], feed_dict={X: x_batch,
#                                                                 Y: y_batch})
#            print("Step " + str(step) + ", Minibatch Loss= " + \
#                  "{:.4f}".format(loss) + ", Training Accuracy= " + \
#                  "{:.3f}".format(acc))
#        try:
#            data_batch= dataset_iter.next()
#        except StopIteration:
#            dataset_iter = tf.Iterator()
#            data_batch = dataset_iter.next()
#
