#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  7 00:01:02 2018

@author: afiny
"""
import numpy as np
import json
from pathlib import Path

"""
    The parameter below controls the features number in a 
    time step of the time series. Change it once we have different input data
"""
features_per_timestep = 18


def get_json_file_path_from_folder( folder_path_string ):
    """
        Feed in a folder, this returns all json file paths in a folder.
        Please make sure that each distinct class is in a different folder.
    """
    pathlist = Path( folder_path_string).glob('**/*.json')
    # change the .json string above for different file endings
    gestures_in_folder =[] 
    for path in pathlist:
        path_in_str = str(path)
        gestures_in_folder.append( path_in_str)
        
    return gestures_in_folder 

def load_json_to_numpy( filename ):
    """
        This takes in a file in the folder that is a gesture,
        and converts it to json
    """
    loaded_list = json.load( open( filename))
    loaded_list =np.array( loaded_list )
    return loaded_list


def cut_gesture_to_a_timestep( numpy_array, required_timestep ):
    
    """
        Takes in variable length array, and outputs a 
        required_timestep length array
    """
    assert numpy_array.shape[0]>=required_timestep 
    # asserts if array length> required_timestep
    
    cut_array = np.delete( numpy_array, 
                           range( required_timestep, len( numpy_array )), 0)
    return cut_array



def change_gesture_to_remove_empty_timesteps( degenerate_numpy_array ):
    """
        Takes in numpy array with few time missing, and returns
        us a numpy array with those timesteps removed.
        NOTE: feeding non-degenerate arrays is buggy.
    """
    bad_index = [] 
    for time_step in range( len ( degenerate_numpy_array ) ) :
        if len( degenerate_numpy_array[time_step] )<features_per_timestep:
            bad_index.append( time_step)
        
    chopped_up_array = np.delete( degenerate_numpy_array, bad_index )
    
    if not chopped_up_array.shape ==(0,):
        chopped_up_array = np.stack( chopped_up_array ) #converts from list to numpy 
        print( chopped_up_array.shape )
        return chopped_up_array
    
def change_gesture_to_pad_0_in_empty_timesteps( degenerate_numpy_array):
    """
        TODO
    """
    pass
#
    
def remove_all_short_gestures( list_of_gestures, minimum_length):
    """
        take in a list of gestures, remove all shorter than minimum length 
    """
    index_to_be_removed = []
    
    for i in range(len(list_of_gestures)-2):
        if list_of_gestures[i].shape[0] < minimum_length:
            index_to_be_removed.append(i)
            
    array_with_short_gesture_removed = np.delete(list_of_gestures, index_to_be_removed)
    list_with_short_gestures_removed = list(array_with_short_gesture_removed)
    return list_with_short_gestures_removed

def update_list_to_remove_empty_timesteps( list_of_gestures, ):
    """
        UNTESTED.
        This function removes empty timesteps if such exist.
        if
    """
    updated_list = []
    for gesture in range(len(list_of_gestures)):
            if len( list_of_gestures[gesture].shape)==2:
                if list_of_gestures[gesture].shape[1]<18:
                    changed_gesture = change_gesture_to_remove_empty_timesteps( 
                            list_of_gestures[gesture])
                    updated_list.append( changed_gesture)
                else:
                    updated_list.append( list_of_gestures[gesture])
                
            if len( list_of_gestures[gesture].shape)==1:
                pass
    filtered_updated_list = list( filter(None.__ne__, updated_list))
    return filtered_updated_list




if __name__ == "__main__":
    
    peace_folder = get_json_file_path_from_folder(
                        "/Users/afiny/Leapmotion_neural_net_prototypes/Tensorflow_models/Peace") 
    peace_gestures = list( map(load_json_to_numpy , peace_folder))
    
    five_fingers_folder = get_json_file_path_from_folder( 
                        "/Users/afiny/Leapmotion_neural_net_prototypes/Tensorflow_models/Five_fingers" )
    five_fingers_gestures = list(map(load_json_to_numpy, five_fingers_folder ) )
    
    thumb_folder = get_json_file_path_from_folder( 
                        "/Users/afiny/Leapmotion_neural_net_prototypes/Tensorflow_models/thumb")
    thumb_gestures = list(map(load_json_to_numpy, thumb_folder ) )
    
    no_pinky_folder = get_json_file_path_from_folder(
                        "/Users/afiny/Leapmotion_neural_net_prototypes/Tensorflow_models/no_pinky") 
    no_pinky_gestures = list(map(load_json_to_numpy, no_pinky_folder ) )

    

    minimum_length = 15
    
    peace_gestures_updated = remove_all_short_gestures(peace_gestures,
                                                       minimum_length)
    five_fingers_gestures_updated = remove_all_short_gestures(five_fingers_gestures,
                                                       minimum_length)
    thumb_gestures_updated = remove_all_short_gestures(thumb_gestures,
                                                       minimum_length)
    
    no_pinky_gestures_updated = remove_all_short_gestures(no_pinky_gestures,
                                                       minimum_length)


    cut_peace_to_timeframe = [cut_gesture_to_a_timestep( 
                                peace_gestures_updated[i], minimum_length )
                                for i in range(len(peace_gestures_updated )-1)]
    
    cut_five_fingers_to_timeframe = [cut_gesture_to_a_timestep( 
                                five_fingers_gestures_updated[i], minimum_length )
                                for i in range(len(five_fingers_gestures_updated )-1)]
    
    cut_thumb_to_timeframe = [cut_gesture_to_a_timestep( 
                                thumb_gestures_updated[i], minimum_length )
                                for i in range(len(thumb_gestures_updated )-1)]
    
    cut_no_pinky_to_timeframe = [cut_gesture_to_a_timestep( 
                                no_pinky_gestures_updated[i], minimum_length )
                                for i in range(len(no_pinky_gestures_updated )-1)]

    
    print(len(cut_peace_to_timeframe), len(cut_five_fingers_to_timeframe),
          len( cut_thumb_to_timeframe),len(cut_no_pinky_to_timeframe) )
    
    
    ### Below we cut the gestures so we have 200 examples each. 
    ### And we save to file.
    
    peace = np.save("peace.npy", cut_peace_to_timeframe )
    five_fingers = np.save("five_fingers.npy", cut_five_fingers_to_timeframe)
    thumb = np.save("thumb.npy", cut_thumb_to_timeframe)
    no_pinky = np.save("no_pinky.npy", cut_no_pinky_to_timeframe)
    
    """
    thumb_200 = np.save("thumb_200.npy", cut_thumb_to_timeframe[0:200] )
    five_fingers_200 = np.save("five_fingers_200.npy", cut_five_fingers_to_timeframe[0:200] )
    peace_200 = np.save( "peace_200.npy", cut_peace_to_timeframe[0:200] )
    """

    
    
    
    
    
    
    ### work with this updated list from now on
    # PROBLEM: WHAT IF SHAPE DOESNT EXIST AS IN five_fingers_GESTURE[63]
    
    
    
    
    