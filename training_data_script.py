################################################################################
# Copyright (C) 2012-2013 Leap Motion, Inc. All rights reserved.               #
# Leap Motion proprietary and confidential. Not for distribution.              #
# Use subject to the terms of the Leap Motion SDK Agreement available at       #
# https://developer.leapmotion.com/sdk_agreement, or another agreement         #
# between Leap Motion and you, your company or other organization.             #
################################################################################

import Leap, sys, thread, time, json
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

import time
timestr = time.strftime("%H%M%S")

"""
   Make sure to change swipeleft to the required gesture
"""
fLeft = open('swipeLeft.json', 'w')
fRight = open('Peace'+timestr+'.json', 'w')
list2FileLeft = []
list2FileRight = []

class SampleListener(Leap.Listener):
    finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
    bone_names = ['Metacarpal', 'Proximal', 'Intermediate', 'Distal']
    state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']

    def on_init(self, controller):
        print "Initialized"

    def on_connect(self, controller):
        print "Connected"

        # Enable gestures
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE);
        controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP);
        controller.enable_gesture(Leap.Gesture.TYPE_SCREEN_TAP);
        controller.enable_gesture(Leap.Gesture.TYPE_SWIPE);

    def on_disconnect(self, controller):
        # Note: not dispatched when running in a debugger.
        print "Disconnected"

    def on_exit(self, controller):
        print "Exited"

    def on_frame(self, controller):

        # Get the most recent frame and report some basic information
        frame = controller.frame()

        # Get hands
        for hand in frame.hands:
            position = []
            handType = "Left hand" if hand.is_left else "Right hand"
            print "["
            print "%f, %f, %f" % (
               hand.palm_position.x, hand.palm_position.y, hand.palm_position.z)
            position.append(hand.palm_position.x)
            position.append(hand.palm_position.y)
            position.append(hand.palm_position.z)

            # Get fingers
            for finger in hand.fingers:
                # Get bones
                bone = finger.bone(2)
                print " ,%f, %f, %f" % (
                    bone.prev_joint.x,
                    bone.prev_joint.y,
                    bone.prev_joint.z)
                position.append(bone.prev_joint.x)
                position.append(bone.prev_joint.y)
                position.append(bone.prev_joint.z)

            print "]"

            if len(frame.hands)==2:
                if hand.is_left:
                    list2FileLeft.append(position)
                if hand.is_right:
                    list2FileRight.append(position)
            else:
                if hand.is_left:
                    list2FileLeft.append(position)
                    list2FileRight.append([])
                if hand.is_right:
                    list2FileRight.append(position)
                    list2FileLeft.append([])
        if not (frame.hands.is_empty and frame.gestures().is_empty):
            print ""


def state_string(self, state):
    if state == Leap.Gesture.STATE_START:
        return "STATE_START"

    if state == Leap.Gesture.STATE_UPDATE:
        return "STATE_UPDATE"

    if state == Leap.Gesture.STATE_STOP:
        return "STATE_STOP"

    if state == Leap.Gesture.STATE_INVALID:
        return "STATE_INVALID"


def main():
    # Create a sample listener and controller
    listener = SampleListener()
    controller = Leap.Controller()

    # Have the sample listener receive events from the controller
    controller.add_listener(listener)

    # Keep this process running until Enter is pressed
    print "Press Enter to quit..."
    try:
        sys.stdin.readline()
    except KeyboardInterrupt:
        pass
    finally:
        # Remove the sample listener when done
        controller.remove_listener(listener)

    fLeft.write(json.dumps(list2FileLeft))
    fRight.write(json.dumps(list2FileRight))


if __name__ == "__main__":
    main()
