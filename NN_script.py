#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  6 00:15:04 2018

@author: afiny
"""
from __future__ import print_function
import numpy as np
import tensorflow as tf
import tensorflow.contrib.eager as tfe


tfe.enable_eager_execution()

# Parameters
learning_rate = 0.001
num_steps = 200
display_step = 25
batch_size = 5


# Network Parameters
n_hidden_1 = 256 # 1st layer number of neurons
n_hidden_2 = 1530 # 2nd layer number of neurons
n_hidden_3 = 1530
#n_hidden_4 = 1530
#n_hidden_5 = 256
num_input = 15*18 #85*18 = minimum_length*18
num_classes = 4 # 
thumb = np.load("thumb.npy")
thumb_labels =  [[0] for i in range(len(thumb))]
#thumb_labels = np.squeeze(thumb_labels)
stacked_thumb = [np.hstack(thumb[i]) for i in range(len(thumb) ) ]

peace = np.load("peace.npy")
peace_labels =  [[1] for i in range(len(peace))]
#peace_labels = np.squeeze(peace_labels)
stacked_peace = [np.hstack(peace[i]) for i in range(len(peace) ) ]


five_fingers = np.load("five_fingers.npy")
five_fingers_labels = [[2] for i in range(len(five_fingers))]   
#five_fingers_labels = np.squeeze(five_fingers_labels)
stacked_five_fingers = [np.hstack(five_fingers[i]) for i in range(len(five_fingers) ) ]


no_pinky = np.load("no_pinky.npy")
no_pinky_labels = [[3] for i in range(len(no_pinky))] 
#no_pinky_labels = np.squeeze(no_pinky_labels)
stacked_no_pinky = [np.hstack(no_pinky[i]) for i in range(len(no_pinky)) ]

###Training Dataset
labels = np.vstack((thumb_labels[0:30], peace_labels[0:30], 
                    five_fingers_labels[0:30], no_pinky_labels[0:30]))
labels = np.squeeze(labels) # 

stacked_features = np.vstack((stacked_thumb[0:30], stacked_peace[0:30], 
                              stacked_five_fingers[0:30], stacked_no_pinky[0:30]))

stacked_features = np.array(stacked_features, dtype = np.float32)


### Test_dataset
stacked_test_features = np.vstack((stacked_thumb[30:-1], stacked_peace[30:-1], 
                              stacked_five_fingers[30:-1], stacked_no_pinky[30:-1]))

test_labels = np.vstack((thumb_labels[30:-1], peace_labels[30:-1], 
                    five_fingers_labels[30:-1], no_pinky_labels[30:-1]))
test_labels = np.squeeze(test_labels)


dataset = tf.data.Dataset.from_tensor_slices( (stacked_features, labels)).shuffle(buffer_size = 40000)
dataset = dataset.batch(batch_size)
dataset_iter = tfe.Iterator(dataset)


# Define the neural network. To use eager API and tf.layers API together,
# we must instantiate a tfe.Network class as follow:
class NeuralNet(tfe.Network):
    def __init__(self):
        super(NeuralNet, self).__init__()
        
        self.layer1 = self.track_layer(
            tf.layers.Dense(n_hidden_1, activation=tf.nn.relu))

        self.layer2 = self.track_layer(
            tf.layers.Dense(n_hidden_2, activation=tf.nn.relu))

        self.layer3 = self.track_layer(
            tf.layers.Dense(n_hidden_3, activation = tf.nn.relu))
       
#        self.layer4 = self.track_layer(
#            tf.layers.Dense(n_hidden_4, activation = tf.nn.relu))
#
#        self.layer5 = self.track_layer(
#            tf.layers.Dense(n_hidden_5, activation = tf.nn.relu))
        # FC layer
        self.out_layer = self.track_layer(tf.layers.Dense(num_classes))

    def call(self, x):
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
#        x = self.layer4(x)
#        x = self.layer5(x)
        return self.out_layer(x)


neural_net = NeuralNet()


# Cross-Entropy loss function
def loss_fn(inference_fn, inputs, labels):
    
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(
        logits=inference_fn(inputs), labels=labels))


#  accuracy
def accuracy_fn(inference_fn, inputs, labels):
    prediction = tf.nn.softmax(inference_fn(inputs))
    correct_pred = tf.equal(tf.argmax(prediction, 1), labels)
    return tf.reduce_mean(tf.cast(correct_pred, tf.float32))



optimizer = tf.train.AdagradOptimizer(learning_rate=learning_rate, initial_accumulator_value=0.1)
# Compute implicit grads
grad = tfe.implicit_gradients(loss_fn)


# Start training
average_loss = 0.
average_acc = 0.
for step in range(num_steps):
    # iter stuff
    try:
        data_batch= dataset_iter.next()
    except StopIteration:
        dataset_iter = tfe.Iterator(dataset)
        data_batch = dataset_iter.next()

    # Depth
    x_batch = data_batch[0]
    # Labels
    y_batch = tf.cast(data_batch[1], dtype=tf.int64)

    # Compute the batch loss
    batch_loss = loss_fn(neural_net, x_batch, y_batch)
    average_loss += batch_loss
    # Compute the batch accuracy
    batch_accuracy = accuracy_fn(neural_net, x_batch, y_batch)
    average_acc += batch_accuracy

    if step == 0:
        # Display the initial cost, before optimizing
        print("Initial loss= {:.9f}".format(average_loss))
        
    # Update the variables following gradients info
    optimizer.apply_gradients(grad(neural_net, x_batch, y_batch))

    # Display info
    if (step + 1) % display_step == 0 or step == 0:
        if step > 0:
            average_loss /= display_step
            average_acc /= display_step
        print("Step:", '%04d' % (step + 1), " loss=",
              "{:.9f}".format(average_loss), " accuracy=",
              "{:.4f}".format(average_acc))
        average_loss = 0.
        average_acc = 0.

# Evaluate model on the test set

average_loss
average_acc
import time
start_time = time.time()
test_acc = accuracy_fn(neural_net,
                       stacked_test_features, test_labels)
end_time = time.time() - start_time 
print(test_acc)
print(end_time)


# print("Testset Accuracy: {:.4f}".format(test_acc))